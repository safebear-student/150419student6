Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful


  Rules:
  The User must be informed if the login info is incorrect
  The User must be informed if the login info is successful

  Glossary:
  User: CRUD - Create,Read, Update, Delete

  Scenario Outline: A user logs into the application
    Given I navigate to the login page
    When  I submit the login details for a '<userType>'
    Then  I can see the following message '<validationMessage>'


    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |


  Scenario: A user logs in and sees the welcome message
    Given I navigate to the login page
    When I submit the login details for a 'validUser'
    Then I can see the following message 'Welcome Tester!'


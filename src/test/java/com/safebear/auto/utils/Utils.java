package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Utils {
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("window-size=1366,768");

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(chromeOptions);
            case "firefox":
                return new FirefoxDriver();
            case "IE":
                return new InternetExplorerDriver();
            case "headless":
                firefoxOptions.addArguments("--headless");
                return new FirefoxDriver(firefoxOptions);

            default:
                return new ChromeDriver(chromeOptions);
        }
    }
}

package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;


    @Before
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }


    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        //Step 1: Action - Spin up homepage
        driver.get(Utils.getUrl());

        //Step 1 result -
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.expectedResult);
        // Step 1 result 2
        Assert.assertNotEquals(loginPage.getPageTitle(), loginPage.wrongResult);
    }

    @When("^I submit the login details for a '(.+)'$")
    public void i_submit_the_login_details_for_a_User(String userType) throws Throwable {
        switch (userType) {
            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();

                break;
            case "invalidUser":
                loginPage.login("testhack", "donothing");
                break;

            default:
                Assert.fail("you logged in with neither the valid or invlaid user statement");
        }

    }

    @Then("^I can see the following message '(.+)'$")
    public void i_can_see_the_following_message_Username(String message) throws Throwable {
        switch (message) {
            case "Username or Password is incorrect":
                Assert.assertEquals(loginPage.validationMsg(), loginPage.validationMsg());
                Assert.assertTrue(loginPage.validationMsg().contains(message));
                break;
            case "Login Successful":
                Assert.assertEquals(toolsPage.checkForLoginSuccessMsg(), toolsPage.checkForLoginSuccessMsg());
                Assert.assertTrue(toolsPage.checkForLoginSuccessMsg().contains(message));


                break;
            case "Welcome Tester!":
                Assert.assertEquals(toolsPage.checkForLoginSuccessMsg(), toolsPage.checkForLoginSuccessMsg());
                Assert.assertTrue(toolsPage.checkForTextMsg().contains(message));

                break;


            default:
                Assert.fail("This message is displayed if neither is True");
                break;
        }
    }


}
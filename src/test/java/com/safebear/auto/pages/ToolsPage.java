package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
@Data
public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();


    @NonNull
    WebDriver driver;

    public String checkForLoginSuccessMsg() {
        return driver.findElement(locators.getLoginSuccessfulLocator()).getText();
    }

    public String checkForTextMsg() {
        return driver.findElement(locators.getLoginWelcomeMessageLocator()).getText();

    }

    private void deleteTrashIcon() {
        driver.findElement(locators.getRemoveTrashLocator()).click();
    }



}

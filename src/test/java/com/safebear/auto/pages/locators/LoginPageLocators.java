package com.safebear.auto.pages.locators;

import lombok.Data;
import lombok.Getter;
import org.openqa.selenium.By;

@Getter
//@Data

public class LoginPageLocators {
    private By usernameFieldLocator = By.id("username");
    private By passwordFieldLocator = By.id("password");
    private By submitButtonLocator = By.id("enter");

    private By validationWarningMsgLocator = By.xpath ("//p[@id='rejectLogin']/b");
}

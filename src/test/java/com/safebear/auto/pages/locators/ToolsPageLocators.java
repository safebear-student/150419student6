package com.safebear.auto.pages.locators;

import lombok.Getter;
import org.openqa.selenium.By;

@Getter
public class ToolsPageLocators {

    private By loginSuccessfulLocator = By.xpath(".//*[contains(text() ,'Login Successful')]");
    private By loginWelcomeMessageLocator = By.cssSelector("div> div:nth-child(3) > h1"); //("//p[@div/div[1]/p/text()]/b");
    private By removeTrashLocator = By.xpath(".//td[contains(text(),'JMeter')]/following-sibling::td/a");
    //private By searchTextLocator = By.name("//h2[.='Search Here']");


}

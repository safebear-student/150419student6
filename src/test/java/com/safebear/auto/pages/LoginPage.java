package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();
    @Getter
    public String expectedResult = "Login Page";
    public String wrongResult = "Not correct";


    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void enterUsername(String username) {
        driver.findElement(locators.getUsernameFieldLocator()).sendKeys(username);

    }

    public void enterPassword(String password) {
        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(locators.getSubmitButtonLocator()).click();
    }

    public void login(String username, String password) {
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
    }

    public String validationMsg() {
        return driver.findElement(locators.getValidationWarningMsgLocator()).getText();
    }
}

package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee() {
        // create Employee called hannah
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        // This is the method to hire Hannah and fire bob
        hannah.employ();
        bob.fire();

        //create salesEmployee and add new car details
        victoria.employ();
        victoria.changeCar("Tesla");
        victoria.givePayRise(2000);

        //This method to print out status of employment
        System.out.println("Hannah's employment status is: " + hannah.isEmployed());
        System.out.println("Bob's employment status is: " + bob.isEmployed());
        System.out.println("Victoria's employment status is " + victoria.isEmployed());
        System.out.println("Victoria's car is a " + victoria.car);
        System.out.println("Victoria's pay rise is " + victoria.payRiseAmt);

    }
}

package com.safebear.auto.syntax;

public class Employee {
    private boolean employed;
    private int salary;
     int payRiseAmt;

    public boolean isEmployed() {
        return employed;
    }

    public void setEmployed() {
        this.employed = employed;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary() {
        this.salary = salary;
    }

    public void fire() {
        employed = false;
    }

    public void employ() {
        employed = true;
    }

    public void givePayRise(int payRiseAmt) {
        salary = salary + payRiseAmt;
        this.payRiseAmt = payRiseAmt;

    }

    }

